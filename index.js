const { start } = require('./Api');
const fs = require('fs')
const path = require('path')
const env = require('dotenv').config();

const CRITERIA_LAST_HOURS = 1;
const CRITERIA_LIKES = 10;
const CRITERIA_QUERY = `async OR "meeting" OR "google meet" OR zoom`;

function check_tweet(tweet) {
           
   /**
    * Criteria 
    */
    let criteriaDate = new Date();
    criteriaDate.setHours(criteriaDate.getHours() - CRITERIA_LAST_HOURS);

    /**
     * Data
     */
    let tweetDate = new Date(tweet.created_at);

    return (tweetDate > criteriaDate);
}

function save_tweet(tweet)
{
    const tweet_path = path.resolve(`./tweets/${tweet.id}.json`);

    if (fs.existsSync(tweet_path)) {
        return;
    }

    fs.writeFileSync(tweet_path, JSON.stringify(tweet)); 

    log_tweet(tweet);
}

function log_tweet(tweet)
{
    console.log('[TWEET ALERT]', tweet.id);
}

/**
 * Twitter tranding scraper – Rate Limits Save
 * @param  {object} options             Query config – https://developer.twitter.com/en/docs/twitter-api/v1/tweets/search/api-reference/get-search-tweets
 * @param  {function} tweets              Tweets handler
 * @return {void}
 */
start({
    q: process.env.API_QUERY, // `(${CRITERIA_QUERY}) min_faves:${CRITERIA_LIKES}`,
    lang: 'en',
    result_type: 'recent',
    count: 100,
}, (tweets) => {

    for (let tweet of tweets) {
        if(!check_tweet(tweet)) {
            continue;
        }

        save_tweet(tweet);
    }

});


