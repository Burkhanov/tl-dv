const needle = require('needle');
const axios = require('axios').default;

const streamURL = 'https://api.twitter.com/1.1/search/tweets.json';

const API_TWEETS_WINDOW_LIMIT = 300; // Real is 450
const API_WINDOW_MINUTES_SIZE = 15; // Don't change

let API_PARAMS = '';
let API_INTERVAL = null;
/**
 * Main
 * @param  {[type]} params [description]
 * @return {[type]}        [description]
 */
async function start(params, handler)
{
    const QUERY_INTERVAL = getTimeout();

    console.log(`Set Query interval: ${QUERY_INTERVAL}ms`);
    // Set init params
    setParams(params);

    API_INTERVAL = setInterval(async () => {
        const response = await findTweets();

        handler(response.statuses);

        // Next results url doesnt work
        // setParams(response.search_metadata.next_results + '&count=100');
    }, QUERY_INTERVAL);
}

/**
 * Helpers
 * @param {[type]} params [description]
 */
function setParams(params)
{
    let query = params;

    if(typeof params === 'object') {
        query = '?' + Object.keys(params)
            .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
            .join('&');
    }

    API_PARAMS = query;
}

function getTimeout(params) {

    const limit_per_minute = (API_TWEETS_WINDOW_LIMIT / API_WINDOW_MINUTES_SIZE);
    const min_minutes_timeout =60 / limit_per_minute;

    // to miliseconds
    return  Math.round(min_minutes_timeout * 1000);
}

/**
 * Request
 * @param  {[type]} params [description]
 * @return {[type]}        [description]
 */
async function findTweets(params = null) {

    if(!params) {
        params = API_PARAMS;
    }

    console.log('API request:', params);

    try {
        const response = await axios.get(streamURL + params, {
            headers: {
                Authorization: `Bearer ${process.env.API_BEARER_TOKEN}`
            }
        });

        return response.data;
    } catch(err) {
        console.error('[ERROR]', err.response.data.errors);
        process.exit();
    }

    return {statuses:[]};
}


module.exports = {
    start,
}