# Get token

```
curl -u "$API_KEY:$API_SECRET_KEY" \
  --data 'grant_type=client_credentials' \
  'https://api.twitter.com/oauth2/token'
```

# Prepare env

.env file should contain
```
API_BEARER_TOKEN=AAAAAAAAAAAAAAAAAAAAAJ39PQAAAAAA2K9%2FKDoz%2Bo%2BTfrG0KxekgjNMN5w%3DPZ3WapwaV0fV99g0IRVhIGOfsyFvihtjyerRb5X8ONUOQaVSF8
API_QUERY='(async OR "meeting" OR "google meet" OR zoom) min_faves:30'
```

# Build container

```
docker build -t tldv-app .
```

# Start container

```
docker run --env-file ./.env tldv-app
```


docker build -t tldv-app . && docker run --env-file ./.env tldv-app
